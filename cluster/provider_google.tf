provider "google" {
	credentials = file("./cred/credentials.json")
	region		= "europe-west6"
	project		= "infra-edge-253513"
}