resource "google_container_cluster"	"primary"{
	name				= "bundle"
	location			= "europe-west6-a"
	initial_node_count	= 4
	
	node_config {
		preemptible  = true
		machine_type = "n1-standard-1"

		metadata = {
			disable-legacy-endpoints = "true"
		}

		oauth_scopes = [
			"https://www.googleapis.com/auth/logging.write",
			"https://www.googleapis.com/auth/monitoring",
		]
	}
}